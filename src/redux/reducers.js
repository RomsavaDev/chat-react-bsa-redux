
import {combineReducers} from "redux";

function setMessagesReducer(state = [], action) {
    switch(action.type) {
        case "SET_MESSAGES" : {
            const {messages} = action;
            return state.concat(messages);
        }
        default : return state;
    }
}

function getMessagesReducer(state = [], action) {
    switch(action.type) {
		case "GET_MESSAGES" : {
			const {messages} = action;
			return state.messages;
	  }
        default: return state;
    }
}

export default combineReducers({
    messages : setMessagesReducer,
    getMessages : getMessagesReducer,
});

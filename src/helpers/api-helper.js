const getMessageFromAPI = async () => {
  const response = await fetch(
    "https://edikdolynskyi.github.io/react_sources/messages.json"
  );
  const data = await response.json();
  return await data;
};

export { getMessageFromAPI };

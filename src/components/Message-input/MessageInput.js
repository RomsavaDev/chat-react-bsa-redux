import React from "react";
import "./MessageInput.css";

export default class MessageInput extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      textareaValue: "",
      message: {
        id: "1234",
        user: "roman",
        avatar: "none",
        text: "",
        createdAt: "2020-07-16T19:48:12.936Z",
      },
    };
  }

  render() {
    return (
      <div className="MessageInput">
        <textarea
          onChange={(e) => {
            this.setState({
              textareaValue: e.target.value,
            });
          }}
          rows="4"
          cols="50"
        ></textarea>
        <button
          onClick={() => {
            if (this.state.textareaValue !== "") {
              this.setState((prevState) => ({
                message: {
                  ...prevState.message,
                  text: this.state.textareaValue,
                },
              }));
              console.log(this.state.message);
              // this.props.addNewMessage(this.state.message)
            }
          }}
        >
          Send
        </button>
      </div>
    );
  }
}

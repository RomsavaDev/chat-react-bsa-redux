import React from "react";
import "./MessageList.css";

export default class MessageList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <ul className="MessageList">
        {this.props.messages.map((item) => {
          return (
            <li className="MessageList-item" key={item.id}>
              <div>{item.user}</div>
              <div>
                <img src={item.avatar}></img>
              </div>
              <div>{item.text}</div>
            </li>
          );
        })}
      </ul>
    );
  }
}
